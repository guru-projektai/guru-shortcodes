<?php

function guru_translate($string)
{
    return apply_filters('wpml_translate_single_string', $string, 'Guru Shortcodes', $string);
}
