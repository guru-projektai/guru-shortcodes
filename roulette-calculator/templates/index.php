<div id="roulette-calculator-<?= $php['id'] ?>" class="roulette-main">
    <div class="roulette-wrapper" style="display: none" v-show="loaded">
        <div class="column align-items-center" v-observe-visibility="{
            callback: visibilityChanged,
            throttle,
            intersection: {
              threshold,
              threshold: 0.75,
            },
          }">
            <label for="calculate" class="input-number-label">
                <?= guru_translate('Enter your bet amount') ?>
            </label>
            <div v-if="! hideGameTypeButtons" class="buttons" style="margin-bottom: 13px">
                <div class="buttons-row">
                    <input
                        :class="`type-button type-button-red ${ isSelectedGameType(37) ? 'selected' : '' }`"
                        type="submit"
                        value="European"
                        @click="selectGameType(37)"
                    />
                    <input
                        :class="`type-button type-button-black ${ isSelectedGameType(38) ? 'selected' : '' }`"
                        type="submit"
                        value="American"
                        @click="selectGameType(38)"
                    />
                </div>
            </div>
            <input class="input-number" type="number" @input="toggle" name="calculate" id="calculate" v-model="amount" />
            <span class="button-select-header"><?= guru_translate('Pick on where you are betting') ?></span>
            <div class="buttons">
                <div class="buttons-row two-in-a-row">
                    <input
                        :class="`type-button type-button-red ${ isSelectedType('red') ? 'selected' : '' } `"
                        type="submit"
                        value="Red"
                        @click="select('red')"
                    />
                    <input
                        :class="`type-button type-button-black ${ isSelectedType('black') ? 'selected' : '' } `"
                        type="submit"
                        value="Black"
                        @click="select('black')"
                    />
                    <input
                        :class="`type-button type-button-blue ${ isSelectedType('odds') ? 'selected' : '' } `"
                        type="submit"
                        value="Odds"
                        @click="select('odds')"
                    />
                    <input
                        :class="`type-button type-button-blue ${ isSelectedType('evens') ? 'selected' : '' } `"
                        type="submit"
                        value="Evens"
                        @click="select('evens')"
                    />
                </div>
                <div class="buttons-row">
                    <input
                        :class="`type-button type-button-green ${ isSelectedType('oneToTwelve') ? 'selected' : '' } `"
                        type="submit"
                        value="1-12"
                        @click="select('oneToTwelve')"
                    />
                    <input
                        :class="`type-button type-button-green ${ isSelectedType('thirteenToTwentyFour') ? 'selected' : '' } `"
                        type="submit"
                        value="13-24"
                        @click="select('thirteenToTwentyFour')"
                    />
                    <input
                        :class="`type-button type-button-green ${ isSelectedType('twentyFiveToThirtySix') ? 'selected' : '' } `"
                        type="submit"
                        value="25-36"
                        @click="select('twentyFiveToThirtySix')"
                    />
                </div>
                <div class="buttons-row different-count">
                    <input
                        :class="`type-button type-button-orange ${ isSelectedType('straight') ? 'selected' : '' } `"
                        type="submit"
                        value="Straight"
                        @click="select('straight')"
                    />
                    <input
                        :class="`type-button type-button-orange ${ isSelectedType('split') ? 'selected' : '' } `"
                        type="submit"
                        value="Split"
                        @click="select('split')"
                    />
                    <input
                        :class="`type-button type-button-orange ${ isSelectedType('street') ? 'selected' : '' } `"
                        type="submit"
                        value="Street"
                        @click="select('street')"
                    />
                    <input
                        :class="`type-button type-button-orange ${ isSelectedType('square') ? 'selected' : '' } `"
                        type="submit"
                        value="Square"
                        @click="select('square')"
                    />
                    <input
                        :class="`type-button type-button-orange ${ isSelectedType('sixline') ? 'selected' : '' } `"
                        type="submit"
                        value="Six Line"
                        @click="select('sixline')"
                    />
                </div>
            </div>
        </div>
        <div class="column bigger-font-size">
            <span class="calculations-title text-center"><?= guru_translate('Calculations') ?></span>
            <span v-if="gameType" :class="`bold game-type-name text-center ${ gameType === 37 ? 'red' : 'black' }`">{{ gameTypeTitle() }}</span>
            <span style="margin-bottom: 5px"><?= guru_translate('Calculate the roulette probability of') ?> <span class="highlight-text">{{ getSelectedType().title }}</span> <?= guru_translate('Bet along with the expected return on your') ?> <span class="highlight-text">{{ amount ? `$${ amount }` : '' }}</span> <?= guru_translate('bet') ?></span>
            <div class="outcome">
                <div class="d-flex" style="margin-bottom: 10px">
                    <div class="column justify-content-center">
                        <span class="bold"><?= guru_translate('Outcome Probability') ?></span>
                    </div>
                    <span class="bold" style="align-self: center; margin-right: 5px">=</span>
                    <div class="column">
                        <div class="equation">
                            <span class="text-center"><span class="red-text">{{ getSelectedTypeValue() }}</span> <?= guru_translate('Numbers') ?></span>
                            <div class="line-hr smaller"></div>
                            <span class="text-center"><span class="red-text text-center">{{ gameType }}</span> <?= guru_translate('Total Numbers') ?></span>
                        </div>
                    </div>
                </div>
                <span style="margin-bottom: 10px"><?= guru_translate('Success Percentage = Outcome Probability x 100%') ?></span>
                <div class="d-flex" style="margin-bottom: 10px">
                    <div class="column flex-row justify-content-center">
                        <span class="bold"><?= guru_translate('Success Percentage') ?></span>
                    </div>
                    <span class="bold" style="align-self: center; margin-right: 5px;">=</span>
                    <div class="bigger-equation justify-content-center">
                        <div class="equation">
                            <span :class="`${ getSelectedTypeValue() ? 'red-text' : '' }`">{{ getSelectedTypeValue() ? getSelectedTypeValue() : 'x' }}</span>
                            <div class="line-hr"></div>
                            <span :class="`${ gameType ? 'red-text' : '' }`">{{ gameType ? gameType : 'x' }}</span>
                        </div>
                        <span style="margin: 0 5px;">x</span>
                        <span style="margin-right: 5px">100</span>
                        <span>=<span v-if="successPercentage()" style="margin-left: 5px" class="highlight-text">{{ successPercentage() }}%</span><span style="margin-left: 5px" v-else>x</span></span>
                    </div>
                </div>
                <div>
                    <?= guru_translate('Expected Return on your Bet = Your Win * (Success Percentage / 100) - 100%') ?>
                </div>
            </div>
        </div>
        <div :class="`w-100 expected-results ${ isVisible ? 'visibility-hidden' : 'visibility-visible' }`">
            <span class="expected-return text-center"><?= guru_translate('Expected Return') ?></span>
            <div class="d-flex justify-content-center align-items-center results">
                <span class="text-center"><?= guru_translate('Success Percentage') ?> =
                    <span v-if="successPercentage()" class="success-percentage">
                        {{ successPercentage() }}%
                    </span>
                </span>
                <span class="text-center"><?= guru_translate('You Win') ?> = <span v-if="winResults()" class="win-results">${{ winResults() }}</span></span>
                <span class="text-center expected-amount-wrapper">
                    <?= guru_translate('Expected Return on your Bet') ?> = <span v-if="expectedReturn()" class="expected-amount">{{ expectedReturn().type }}${{ expectedReturn().amount }}</span>
                </span>
            </div>
        </div>
        <div :class="`w-100 expected-results display-hidden position-fixed ${ isVisible ? 'display-visible' : 'display-hidden' }`">
            <span class="expected-return text-center"><?= guru_translate('Expected Return') ?></span>
            <div class="d-flex justify-content-center align-items-center results">
                <span class="text-center"><?= guru_translate('Success Percentage') ?> =
                    <span v-if="successPercentage()" class="success-percentage" style="color: white">
                        {{ successPercentage() }}%
                    </span>
                </span>
                <span class="text-center"><?= guru_translate('You Win') ?> = <span v-if="winResults()" class="win-results">${{ winResults() }}</span></span>
                <span class="text-center expected-amount-wrapper">
                    <?= guru_translate('Expected Return on your Bet') ?> = <span v-if="expectedReturn()" class="expected-amount">{{ expectedReturn().type }}${{ expectedReturn().amount }}</span>
                </span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    new Vue({
        el: `#roulette-calculator-<?= $php['id'] ?>`,
        data: {
            loaded: false,
            selectedType: 'red',
            amount: 100,
            gameType: 37,
            gameTypeMap: {
                37: 'European Roulette',
                38: 'American Roulette',
            },
            phpId: '<?= $php['id'] ?>',
            phpHideButtons: '<?= $php['hideButtons'] ?>',
            phpGameType: '<?= $php['gameType'] ?>',
            hideGameTypeButtons: false,
            typeMap: {
                red: {
                    value: {
                        37: 18,
                        38: 18,
                    },
                    title: 'Red',
                    payoutMultiply: 2
                },
                black: {
                    value: {
                        37: 18,
                        38: 18,
                    },
                    title: 'Black',
                    payoutMultiply: 2
                },
                odds: {
                    value: {
                        37: 18,
                        38: 18,
                    },
                    title: 'Odds',
                    payoutMultiply: 2
                },
                evens: {
                    value: {
                        37: 18,
                        38: 18,
                    },
                    title: 'Evens',
                    payoutMultiply: 2
                },
                oneToTwelve: {
                    value: {
                        37: 12,
                        38: 12,
                    },
                    title: '1-12',
                    payoutMultiply: 3
                },
                thirteenToTwentyFour: {
                    value: {
                        37: 12,
                        38: 12,
                    },
                    title: '13-24',
                    payoutMultiply: 3
                },
                twentyFiveToThirtySix: {
                    value: {
                        37: 12,
                        38: 12,
                    },
                    title: '25-36',
                    payoutMultiply: 3
                },
                straight: {
                    value: {
                        37: 1,
                        38: 1,
                    },
                    title: 'Straight',
                    payoutMultiply: 36
                },
                split: {
                    value: {
                        37: 2,
                        38: 2,
                    },
                    title: 'Split',
                    payoutMultiply: 18
                },
                street: {
                    value: {
                        37: 3,
                        38: 3,
                    },
                    title: 'Street',
                    payoutMultiply: 12
                },
                square: {
                    value: {
                        37: 4,
                        38: 4,
                    },
                    title: 'Square',
                    payoutMultiply: 9
                },
                sixline: {
                    value: {
                        37: 6,
                        38: 6,
                    },
                    title: 'Six Line',
                    payoutMultiply: 6
                }
            },
            throttle: 0,
            threshold: 0,
            isVisible: false,
        },
        created() {
            this.loaded = true

            if (this.phpGameType.length > 0) {
                switch (this.phpGameType) {
                    case 'American':
                        this.gameType = 38
                        break
                    case 'Europe':
                        this.gameType = 37
                        break
                }
            }

            this.hideGameTypeButtons = this.phpHideButtons.length > 0 ? this.phpHideButtons : false
        },
        methods: {
            visibilityChanged (isVisible, entry) {
                this.isVisible = isVisible
            },
            isSelectedType(type) {
                return this.selectedType === type
            },
            isSelectedGameType(gameType) {
                return this.gameType === gameType
            },
            toggle($event) {
                this.amount = Number($event.target.value)
            },
            select(label) {
                this.selectedType = label
            },
            getSelectedType() {
                if (! this.selectedType) return ''

                return this.typeMap[this.selectedType]
            },
            getSelectedTypeValue() {
                if (! this.selectedType || ! this.gameType) return ''

                return this.getSelectedType().value[this.gameType]
            },
            selectGameType(gameType) {
                this.gameType = gameType
            },
            gameTypeTitle() {
                if (! this.gameType) return ''

                return this.gameTypeMap[this.gameType]
            },
            successPercentage() {
                if (! this.selectedType || ! this.gameType) return ''

                return ((this.getSelectedTypeValue() / this.gameType) * 100).toFixed(2)
            },
            winResults() {
                if (! this.selectedType || ! this.gameType || ! this.amount) return ''

                return Number((this.amount * this.getSelectedType().payoutMultiply).toFixed(2))
            },
            expectedReturn() {
                if (! this.selectedType || ! this.gameType || ! this.amount) return ''

                let betAmount = this.amount

                let amount = (this.winResults()) * (this.successPercentage() / 100) - betAmount

                const color = amount > 0 ? 'green' : 'red'

                let type = amount > 0 ? 1 : -1

                if (type < 0) amount *= type

                return {
                    amount: amount.toFixed(2),
                    color,
                    type: type < 0 ? '-' : ''
                }
            }
        }
    })
</script>
