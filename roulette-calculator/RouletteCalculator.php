<?php

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'RouletteCalculator_FILE', __FILE__ );
define( 'RouletteCalculator_PATH', plugin_dir_path(RouletteCalculator_FILE) );
define( 'RouletteCalculator_URL', plugin_dir_url(RouletteCalculator_FILE) );

class RouletteCalculator
{
    public function __construct()
    {
        $this->includes();

        add_shortcode('roulette-calculator', [$this, 'shortcode']);
    }

    public function shortcode($attrs = '')
    {
        $shortcodeValue = shortcode_atts([
            'game-type'    => false,
            'hide-buttons' => false,
            'id'           => 1,
        ], $attrs);

        $gameType = ! empty($shortcodeValue['game-type']) ? $shortcodeValue['game-type'] : false;
        $hideButtons = ! empty($shortcodeValue['hide-buttons']) ? $shortcodeValue['hide-buttons'] : false;
        $id = ! empty($shortcodeValue['id']) ? $shortcodeValue['id'] : 1;

        wp_enqueue_script('intersection-observer', 'https://unpkg.com/intersection-observer@0.5.0', ['vue-js']);
        wp_enqueue_script('vue-js', 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js');
        wp_enqueue_script('vue-observe-visibility', 'https://unpkg.com/vue-observe-visibility@0.4.2', ['vue-js']);
        wp_enqueue_style('roulette-calculator-app-styles', plugin_dir_url( __FILE__ ). 'assets/styles.css');

        $php = [
            'gameType' => $gameType,
            'hideButtons' => $hideButtons,
            'id' => $id,
        ];

        ob_start();
        require RouletteCalculator_PATH . 'templates/index.php';
        ob_get_contents();

        return ob_get_clean();
    }

    private function includes()
    {
        require_once RouletteCalculator_PATH . 'includes/functions.php';
    }
}
