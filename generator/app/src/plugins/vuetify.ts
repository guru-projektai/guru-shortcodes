import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#3f81ff",
        secondary: "#1a163d",
        accent: "#d7e3ff",
        success: "#00941B",
        warning: "#E66F00",
        error: "#F1002B"
      }
    }
  }
});
