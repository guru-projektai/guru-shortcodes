import TextField from '@/shared/components/form/field-types/TextField.vue'
import SelectField from '@/shared/components/form/field-types/SelectField.vue'
import SearchableField from '@/shared/components/form/field-types/SearchableField.vue';
import CheckboxField from '@/shared/components/form/field-types/CheckboxField.vue';
import SwitchField from '@/shared/components/form/field-types/SwitchField.vue';
import EditorField from '@/shared/components/form/field-types/EditorField.vue'
import NumberField from '@/shared/components/form/field-types/NumberField.vue'

export enum FieldTypes {
  text = 'text',
  number = 'number',
  select = 'select',
  searchable = 'searchable',
  checkbox = 'checkbox',
  switch = 'switch',
  editor = 'editor',
}

export const fieldTypesMap: any = {
  [FieldTypes.text]: TextField,
  [FieldTypes.number]: NumberField,
  [FieldTypes.select]: SelectField,
  [FieldTypes.searchable]: SearchableField,
  [FieldTypes.checkbox]: CheckboxField,
  [FieldTypes.switch]: SwitchField,
  [FieldTypes.editor]: EditorField,
}
