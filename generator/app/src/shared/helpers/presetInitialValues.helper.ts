import _ from 'lodash'
import FormBase from '@/shared/classes/form/form-base'
import Field from '@/shared/classes/form/field'
_.noConflict()

const setDefaultFieldValue = (field: any, form: FormBase | null = null, entry: any = null, initialValues: any = null) => {
  let value: any = null

  initialValues = initialValues ? initialValues : form?.initialValues
  if (form) value = _.get(initialValues, field.key, null)
  if (entry) value = _.get(entry, field.key, null)

  switch (field.type) {
    default:
      if (value === null) value = ''
      break
  }

  return value
}

const presetDefaultValues = (form: FormBase): any => {
  const data = {}

  form.fields.forEach((field: Field) => {
    const value = setDefaultFieldValue(field, form)

    _.set(data, field.key, value)
  })

  return data
}

const presetInitialFormValues = async (form: FormBase) => {
  form.setData(presetDefaultValues(form))
}

export default presetInitialFormValues
