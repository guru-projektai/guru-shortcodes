export const generateUrl = (prefix: string, route: string) => {
  const origin = window.location.origin
  return `${ origin }${ prefix }${ route }`
  // return `https://bedrock.lndo.site${ prefix }${ route }`
}
