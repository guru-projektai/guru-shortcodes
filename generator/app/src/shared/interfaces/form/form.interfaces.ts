export interface ILoadItemsProps {
  endpoint?: string
  value?: string
  title?: string
  placeholder?: {
    value: any
    title: any
  }
}
