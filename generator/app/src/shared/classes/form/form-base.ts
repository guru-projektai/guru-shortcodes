import Field from '@/shared/classes/form/field'

export default class FormBase {
  formId: string = Math.random().toString(36).slice(2)
  fields: Field[] = []
  data: any = {}
  injectValues!: any
  initialValues: any = {}
  onChange!: (data: any) => void

  setFields(fields: Field[]): this {
    this.fields = fields
    return this
  }

  setData(data: any): this {
    this.data = data
    return this
  }

  setInjectValues(injectValues: any): this {
    this.injectValues = injectValues
    return this
  }

  setInitialValues(initialValues: any): this {
    this.initialValues = initialValues
    return this
  }

  setOnChange(onChange: (data: any) => void): this {
    this.onChange = onChange
    return this
  }
}
