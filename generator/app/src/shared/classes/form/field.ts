import { FieldTypes } from '@/shared/components/form/field-types';

export enum FieldSizes {
  full = 'full',
  half = 'half',
  quarter = 'quarter',
}

export default class Field {
  type: FieldTypes = FieldTypes.text
  size: FieldSizes = FieldSizes.full
  key: string = ''
  shortcodeKey: string = ''
  title!: string | null
  classes: string = ''
  value: any = ''
  visibleIf: (values: any, index: number|null) => boolean = () => true
  visibleIfAdditional: boolean = false
  onChange!: (value: any, key: any) => void

  setType(type: FieldTypes): this {
    this.type = type
    return this
  }

  setSize(size: FieldSizes): this {
    this.size = size
    return this
  }

  setKey(key: string): this {
    this.key = key
    return this
  }

  setShortcodeKey(key: string): this {
    this.shortcodeKey = key
    return this
  }

  setTitle(title: string): this {
    this.title = title
    return this
  }

  setClasses(classes: string): this {
    this.classes = classes
    return this
  }

  setValue(value: any): this {
    this.value = value
    return this
  }

  setVisibleIf(visibleIf: (values: any, index: number|null) => boolean): this {
    this.visibleIf = visibleIf
    return this
  }

  setVisibleIfAdditional(value: boolean): this {
    this.visibleIfAdditional = value
    return this
  }

  setOnChange(onChange: (value: any, key: any) => void): this {
    this.onChange = onChange
    return this
  }
}
