import { Component, Prop, Vue } from 'vue-property-decorator'
import FormBase from '@/shared/classes/form/form-base'
import Field from '@/shared/classes/form/field'
import _ from 'lodash'
_.noConflict()

@Component
export default class AbstractField extends Vue {
  @Prop() field!: Field | any
  @Prop() form!: FormBase

  get value() {
    if (this.form) return _.get(this.form.data, this.field.key, this.field.value)

    return this.field.value
  }

  set value(value) {
    if (this.form) {
      _.set(this.form.data, this.field.key, value)
      if (this.field.onChange) this.field.onChange(value, this.field.key)

      return
    }

    this.field.value = value
  }
}
