import Field from '@/shared/classes/form/field'
import { FieldTypes } from '@/shared/components/form/field-types'
import SelectItem from '@/shared/classes/fields/select/select-item';

export default class SelectField extends Field {
  type: FieldTypes = FieldTypes.select
  items: SelectItem[] = []

  setItems(items: SelectItem[]): SelectField {
    this.items = items
    return this
  }
}
