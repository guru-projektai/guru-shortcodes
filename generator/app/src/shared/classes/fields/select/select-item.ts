export default class SelectItem {
  value!: string
  title!: string
  type!: string
  appendToTitle: boolean = false

  setValue(value: any): this {
    this.value = value
    return this
  }

  setTitle(title: string): this {
    this.title = title
    return this
  }

  setType(type: string): this {
    this.type = type
    return this
  }

  setAppendToTitle(append: boolean): this {
    this.appendToTitle = append
    return this
  }
}
