import Field from '@/shared/classes/form/field'
import { FieldTypes } from '@/shared/components/form/field-types'
import { ILoadItemsProps } from '@/shared/interfaces/form/form.interfaces'
import SelectItem from '@/shared/classes/fields/select/select-item';

export default class SearchableField extends Field {
  type: FieldTypes = FieldTypes.searchable
  items: SelectItem[] = []
  loadItemsProps!: ILoadItemsProps
  afterSelect!: (value: any) => void

  loadItems(loadItemsProps: ILoadItemsProps): this {
    this.loadItemsProps = loadItemsProps
    return this
  }

  setAfterSelect(afterSelect: (value: any) => void): this {
    this.afterSelect = afterSelect
    return this
  }

  setItems(items: SelectItem[]): this {
    this.items = items
    return this
  }
}
