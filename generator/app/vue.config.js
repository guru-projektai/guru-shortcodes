module.exports = {
  filenameHashing: false,
  publicPath: "./",
  outputDir: "build",
  devServer: {
    writeToDisk: true,
    host: "0.0.0.0",
    hot: true,
    disableHostCheck: true,
    https: false
  },
  productionSourceMap: false,
  transpileDependencies: ["vuetify"]
};
