<?php

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'Shortcode_Generator_FILE', __FILE__ );
define( 'Shortcode_Generator_PATH', plugin_dir_path(Shortcode_Generator_FILE) );
define( 'Shortcode_Generator_URL', plugin_dir_url(Shortcode_Generator_FILE) );

class ShortcodeGenerator
{
    public function __construct()
    {
        $this->includes();
        add_action( 'media_buttons', [$this, 'mediaButton'], 12);
    }

    public function mediaButton()
    {
        $this->loadApp();
    }

    public function loadApp()
    {
//        wp_register_script( 'my-vue-app-vendors',
//            Shortcode_Generator_URL . 'app/build/js/chunk-vendors.js',
//            array(),
//            '1.0.0',
//            true
//        );
//
//        wp_register_script( 'my-vue-app',
//            Shortcode_Generator_URL . 'app/build/js/app.js',
//            array( 'my-vue-app-vendors' ),
//            '1.0.0',
//            true
//        );
//
//        wp_localize_script('my-vue-app-vendors', 'php', []);
//        wp_localize_script('my-vue-app-vendors', 'php', []);
//
//        wp_enqueue_script('my-vue-app-vendors');
//        wp_enqueue_script('my-vue-app');

//        wp_enqueue_style( 'my-vue-app',
//            plugins_url( '/app/build/css/app.css', __FILE__),
//            array(),
//            '1.0.0'
//        );

        wp_enqueue_script('shortcode-generator-app-vendors', Shortcode_Generator_URL . 'app/build/js/chunk-vendors.js', [], '1.0.0', true);
        wp_enqueue_script('shortcode-generator-app', Shortcode_Generator_URL . 'app/build/js/app.js', ['shortcode-generator-app-vendors'], '1.0.0', true);

        wp_enqueue_style('shortcode-generator-app-css', Shortcode_Generator_URL . 'app/build/css/app.css', [], '1.0.0');
        wp_enqueue_style('shortcode-generator-app-vendors-css', Shortcode_Generator_URL . 'app/build/css/chunk-vendors.css', [], '1.0.0');

        echo '<div id="app"></div>';
    }

    private function includes()
    {
        require_once Shortcode_Generator_PATH . 'includes/generator-functions.php';
    }
}
