<?php

function get_sport_branches($request)
{
    $pods = pods('sport_branch');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_bookmakers($request)
{
    $pods = pods('bookmaker');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_licenses($request)
{
    $pods = pods('license');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_deposit_methods($request)
{
    $pods = pods('payment_method');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_cryptocurrencies($request)
{
    $pods = pods('cryptocurrency');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_bookmaker_types($request)
{
    $pods = pods('bookmaker_type');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_betting_selections($request)
{
    $pods = pods('betting_selection');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_products($request)
{
    $pods = pods('product');

    $select = [
        't.name as title',
        't.name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_crypto_currencies($request)
{
    $pods = pods('cryptocurrency');

    $select = [
        't.short_name as title',
        't.short_name as value',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.name ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_guru_sports_data_leagues($request)
{
    $pods = pods(GURU_SPORTS_DATA_LEAGUES_TABLE);

    $select = [
        't.league_title as title',
        't.league_title as value',
        't.id as id',
    ];

    $pods_params = array(
        'select' => $select,
        'distinct' => false,
        'orderby' => 't.league_title ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);
    $data = $pods->data();

    return [
        'data' => $data,
    ];
}

function get_guru_sports_data_league($request)
{
    $league_title = $request->get_param('league_title');

    return [
        'seasons' => get_seasons_by_league_title($league_title),
        'teams' => get_teams_by_league_title($league_title),
    ];
}

function get_seasons_by_league_title($league_title)
{
    $pods = pods(GURU_SPORTS_DATA_LEAGUES_SEASONS_TABLE);

    $select = [
        't.season_title as title',
        't.season_title as value',
    ];

    $pods_params = array(
        'select' => $select,
        'where' => "t.league_title = '$league_title'",
        'distinct' => false,
        'orderby' => 't.season_title ASC',
        'limit' => '-1',
    );

    $pods->find($pods_params);

    return $pods->data();
}

function get_teams_by_league_title($league_title)
{
    $pods = pods(GURU_SPORTS_DATA_EVENTS_TABLE);

    $select = [
        't.home_team as home_team',
        't.away_team as away_team',
    ];

    $pods_params = array(
        'select' => $select,
        'where' => "t.league_title = '$league_title'",
        'distinct' => false,
        'limit' => '-1',
    );

    $pods->find($pods_params);

    $data = $pods->data();
    $teams = [];

    if (! $data) return [];

    foreach ($data as $item) {
        $teams[] = $item->home_team;
        $teams[] = $item->away_team;
    }

    return array_values(array_unique($teams));
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'guru-api/v1', '/sport-branches', [
        'methods' => 'GET',
        'callback' => 'get_sport_branches',
    ]);
    register_rest_route( 'guru-api/v1', '/bookmakers', [
        'methods' => 'GET',
        'callback' => 'get_bookmakers',
    ]);
    register_rest_route( 'guru-api/v1', '/licenses', [
        'methods' => 'GET',
        'callback' => 'get_licenses',
    ]);
    register_rest_route( 'guru-api/v1', '/deposit-methods', [
        'methods' => 'GET',
        'callback' => 'get_deposit_methods',
    ]);
    register_rest_route( 'guru-api/v1', '/cryptocurrencies', [
        'methods' => 'GET',
        'callback' => 'get_cryptocurrencies',
    ]);
    register_rest_route( 'guru-api/v1', '/bookmaker-types', [
        'methods' => 'GET',
        'callback' => 'get_bookmaker_types',
    ]);
    register_rest_route( 'guru-api/v1', '/betting-selections', [
        'methods' => 'GET',
        'callback' => 'get_betting_selections',
    ]);
    register_rest_route( 'guru-api/v1', '/products', [
        'methods' => 'GET',
        'callback' => 'get_products',
    ]);
    register_rest_route( 'guru-api/v1', '/crypto-currencies', [
        'methods' => 'GET',
        'callback' => 'get_crypto_currencies',
    ]);
    register_rest_route( 'guru-api/v1', '/guru-sports-data-leagues', [
        'methods' => 'GET',
        'callback' => 'get_guru_sports_data_leagues',
    ]);
    register_rest_route( 'guru-api/v1', '/guru-sports-data-league', [
        'methods' => 'GET',
        'callback' => 'get_guru_sports_data_league',
    ]);
});




