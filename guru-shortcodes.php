<?php
/*
Plugin Name: Guru Shortcodes
Plugin URI:  https://bitbucket.org/guru-projektai/guru-shortcodes
Description: Various Guru shortcodes for multi-project use.
Version:     0.1.0
Author:      Paulius Pazdrazdys
Author URI:  https://bitbucket.org/Paulens/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: Guru Shortcodes
*/

if (!defined('ABSPATH')) {
    die;
}

define('Guru_Shortcodes_FILE', __FILE__);
define('Guru_Shortcodes_PATH', plugin_dir_path(Guru_Shortcodes_FILE));
define('Guru_Shortcodes_URL', plugin_dir_url(Guru_Shortcodes_FILE));

require_once plugin_dir_path(Guru_Shortcodes_FILE) . 'roulette-calculator/RouletteCalculator.php';
require_once plugin_dir_path(Guru_Shortcodes_FILE) . 'generator/ShortcodeGenerator.php';

new RouletteCalculator;
new ShortcodeGenerator;

/**
 * Guru Shortcodes: [guru-show-more] shortcode.
 */
function guru_shortcodes_show_more($attr, $content)
{
    global $sitepress;
    $current_lang = 'en';
    if (isset($sitepress)) {
        $current_lang = $sitepress->get_current_language();
    }
    if (!empty($sitepress)) {
        $show_more = (get_option('show_more__guru_' . $current_lang)) ? get_option('show_more__guru_' . $current_lang) : 'Show More';
        $show_less = (get_option('show_less__guru_' . $current_lang)) ? get_option('show_less__guru_' . $current_lang) : 'Show Less';
    }
    else {
        $show_more = (get_option('show_more__guru')) ? get_option('show_more__guru') : 'Show More';
        $show_less = (get_option('show_less__guru')) ? get_option('show_less__guru') : 'Show Less';
    }

    if (empty($attr)) {
        $attr = array();
    }

    if (!isset($attr['desktop-limit'])) $attr['desktop-limit'] = '290';
    if (!isset($attr['mobile-limit'])) $attr['mobile-limit'] = '200';
    if (!isset($attr['cut-words'])) $attr['cut-words'] = 'no';
    if (!isset($attr['align'])) $attr['align'] = 'left';
    if (!isset($attr['more'])) $attr['more'] = $show_more . ' <span class="icon"><i class="fas fa-chevron-down"></i></span>';
    if (!isset($attr['less'])) $attr['less'] = $show_less . ' <span class="icon"><i class="fas fa-chevron-up"></i></span>';

    $initial_content = do_shortcode($content);
    $full_content = wpautop(do_shortcode($content));

    $desktop_limit = (int)$attr['desktop-limit'];
    $mobile_limit = (int)$attr['mobile-limit'];

    $cut_words = $attr['cut-words'] == 'yes' ? 'true' : 'false';

    $show_more = 'false';
    $desktop_cut_content = $mobile_cut_content = $full_content;

    $text_length = strlen(strip_tags($full_content));

    if ($text_length > $desktop_limit) { // for desktop
        $desktop_cut_content = truncate_html($cut_words, $full_content, $desktop_limit);
        $show_more = 'true';
    }

    if ($text_length > $mobile_limit) { // for desktop
        $mobile_cut_content = truncate_html($cut_words, $full_content, $mobile_limit);
        $show_more = 'true';
    }
    $guru_show_more_string = '<div class="guru-show-more guru-show-more-desktop">';
    $guru_show_more_string .= $desktop_cut_content;
    $guru_show_more_string .= '<a class="button" href="#" data-show-more="1">'.$attr['more'].'</a>';
    $guru_show_more_string .= '</div>';



    $guru_show_more_string .= '<div class="guru-show-more guru-show-more-mobile">';
    $guru_show_more_string .= $mobile_cut_content;
    $guru_show_more_string .= '<a class="button" href="#" data-show-more="1">'.$attr['more'].'</a>';
    $guru_show_more_string .= '</div>';

    $guru_show_more_string .= '<div class="guru-show-more guru-show-more-full">';
    $guru_show_more_string .= $full_content;
    $guru_show_more_string .= '<a class="button" href="#" data-show-less="1">'.$attr['less'].'</a>';
    $guru_show_more_string .= '</div>';


    return $guru_show_more_string;
}

add_shortcode('guru-show-more', 'guru_shortcodes_show_more');


/**
 * Guru Shortcodes: scripts and styles.
 */
function guru_shortcodes_scripts()
{
    wp_enqueue_style(
        'guru-shortcodes-style',
        Guru_Shortcodes_URL . 'guru-shortcodes.css'
    );
    wp_enqueue_script(
        'guru-jquery-truncate',
        Guru_Shortcodes_URL . 'jquery.truncate.js',
        array('jquery'),
        '0.1.0',
        true
    );
    wp_enqueue_script(
        'guru-shortcodes-script',
        Guru_Shortcodes_URL . 'guru-shortcodes.js',
        array('jquery'),
        '0.1.0',
        true
    );
}

add_action('wp_enqueue_scripts', 'guru_shortcodes_scripts');
add_action('wp_footer', 'guru_shortcodes_scripts');


/**
 * REMOVE NON-BREAKING SPACES FROM POST CONTENT
 * Addresses bug affecting TinyMCE in Chrome and Brave other browsers
 * handles $content already treated by Next Gen Gallery et al
 * also works with wp_insert_post_data, as per answer by Rarst at
 * http://wordpress.stackexchange.com/questions/168356/how-to-stop-wordpress-from-saving-utf8-non-breaking-space-characters
 * set 99 execution priority so after default filters have "fired"
 **/

function custom_remove_buggy_nbsps($content)
{
    // Strip both types of non-breaking space in case NGG or similar is installed.
    $content = str_replace('&nbsp;', ' ', $content);
    $content = str_replace('\xc2\xa0', ' ', $content);

    return $content;
}

add_filter('content_save_pre', 'custom_remove_buggy_nbsps', 99);

//Take in html string, cut it to the supplied limit, but not count style tags while final results includes it.
function truncate_html($cut_words, $text, $length, $ending = '..', $exact = false, $considerHtml = true) {
    if ($cut_words == 'true') {
        $updated_html_string = trim(substr(strip_tags($text), 0, $length));
    } else {
        if ($considerHtml) {
            // if the plain text is shorter than the maximum length, return the whole text
            if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            // splits all html-tags to scanable lines
            preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
            $total_length = strlen($ending);
            $open_tags = array();
            $truncate = '';
            foreach ($lines as $line_matchings) {
                // if there is any html-tag in this line, handle it and add it (uncounted) to the output
                if (!empty($line_matchings[1])) {
                    // if it's an "empty element" with or without xhtml-conform closing slash
                    if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                        // do nothing
                    // if tag is a closing tag
                    } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                        // delete tag from $open_tags list
                        $pos = array_search($tag_matchings[1], $open_tags);
                        if ($pos !== false) {
                        unset($open_tags[$pos]);
                        }
                    // if tag is an opening tag
                    } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                        // add tag to the beginning of $open_tags list
                        array_unshift($open_tags, strtolower($tag_matchings[1]));
                    }
                    // add html-tag to $truncate'd text
                    $truncate .= $line_matchings[1];
                }
                // calculate the length of the plain text part of the line; handle entities as one character
                $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
                if ($total_length+$content_length> $length) {
                    // the number of characters which are left
                    $left = $length - $total_length;
                    $entities_length = 0;
                    // search for html entities
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                        // calculate the real length of all entities in the legal range
                        foreach ($entities[0] as $entity) {
                            if ($entity[1]+1-$entities_length <= $left) {
                                $left--;
                                $entities_length += strlen($entity[0]);
                            } else {
                                // no more characters left
                                break;
                            }
                        }
                    }
                    $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
                    // maximum lenght is reached, so get off the loop
                    break;
                } else {
                    $truncate .= $line_matchings[2];
                    $total_length += $content_length;
                }
                // if the maximum length is reached, get off the loop
                if($total_length>= $length) {
                    break;
                }
            }
        } else {
            if (strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = substr($text, 0, $length - strlen($ending));
            }
        }
        // if the words shouldn't be cut in the middle...
        if (!$exact) {
            // ...search the last occurance of a space...
            $spacepos = strrpos($truncate, ' ');
            if (isset($spacepos)) {
                // ...and cut the text in this position
                $truncate = substr($truncate, 0, $spacepos);
            }
        }
        // add the defined ending to the text
        $truncate .= $ending;
        if($considerHtml) {
            // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }
        
        // Remove all the paragraphs except the first and last ones
        $updated_html_string = str_replace(['<p>', '</p>'], '', $truncate);
        $updated_html_string = trim($updated_html_string);
    }

	return $updated_html_string;
}