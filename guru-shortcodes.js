var $ = jQuery.noConflict();

(function($) {
  var is_mobile_screen = $(window).width() < 767;

  // var cut_words = $('.guru-show-more').data('cut-words') != 'true';
  // var desktop_limit = $('.guru-show-more').data('desktop-limit');
  // var mobile_limit = $('.guru-show-more').data('mobile-limit');
  // //var content_limit = is_mobile_screen ? mobile_limit : desktop_limit;
  //
  // var content_raw = $('.guru-show-more-full').length ? $('.guru-show-more-full').text().trim() : false;
  // var content_html_raw = $('.guru-show-more-full').length ? $('.guru-show-more-full').html().trim() : false;
  // var shortened_html_desktop = $('.guru-show-more-desktop').length ? $('.guru-show-more-desktop').html().trim() : false;
  // var shortened_html_mobile = $('.guru-show-more-mobile').length ? $('.guru-show-more-mobile').html().trim() : false;
  // // if (content_raw) {
  // //   content_raw = ( content_raw.slice( -1 ) === '.' ) ? content_raw = content_raw.slice( 0, -1 ) : content_raw;
  // // }
  //
  // var show_more_button = $('.guru-show-more').data('show-more');
  // var show_more_button_prefix = '<a class="button" href="#" data-show-more="1">';
  // var show_less_button_prefix = '<a class="button" href="#" data-show-less="1">';
  // var show_button_suffix = '</a>';
  // var show_more_html = show_more_button_prefix + $('.guru-show-more').data('show-more-text') + show_button_suffix;
  // var show_less_html = show_less_button_prefix + $('.guru-show-more').data('show-less-text') + show_button_suffix;

  // Shorten HTML - remove line breaks, add ellipsis,
  // break words and truncate to selected length.
  // var shortened_html = jQuery.truncate(content_html_raw, {
  //   length: content_limit,
  //   noBreaks: true,
  //   ellipsis: '..',
  //   words: cut_words,
  //   stripTags: true,
  // });
  //shortened_html = shortened_html.replace('...', '..');
  // shortened_html_desktop = '<p class="initial">' + shortened_html_desktop + '</p>';
  // shortened_html_mobile = '<p class="initial">' + shortened_html_mobile + '</p>';
  // If we need to show more button and
  // we have shortened html - replace current html.
  // if (show_more_button && shortened_html_desktop && shortened_html_mobile) {
  //   $('.guru-show-more-desktop').html(shortened_html_desktop);
  //   $('.guru-show-more-desktop').append(show_more_html);
  //   $('.guru-show-more-mobile').html(shortened_html_mobile);
  //   $('.guru-show-more-mobile').append(show_more_html);
  // }

  // Show guru-show-more content.
  //$('.guru-show-more').removeClass('hide');

  // Show more / show less JS click event.
  $(document).on('click', '.guru-show-more a.button', function(e) {
    e.preventDefault();
    if ($(this).data('show-less')) {
      if (is_mobile_screen) {
        $('.guru-show-more-mobile').show();
      } else {
        $('.guru-show-more-desktop').show();
      }
      $('.guru-show-more-full').hide();
    }
    if ($(this).data('show-more')) {
      if (is_mobile_screen) {
        $('.guru-show-more-mobile').hide();
      } else {
        $('.guru-show-more-desktop').hide();
      }
      $('.guru-show-more-full').show();
    }
  });

})(jQuery);
